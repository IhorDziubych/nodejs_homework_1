const express = require('express')
const fs = require('fs')
const path = require('path')
const router = express.Router()

let filesArr = require('./files')
let dirFiles

fs.readdir('api', function (err, files) {
  if (err) {
    fs.mkdir(path.join(__dirname, 'api/files'), { recursive: true }, (err) => {
      if (err) {
        throw err
      }
    })
  }
})

router.get('/:filename/:password', (req, res) => {
  let birthtime
  fs.readdir(`api/files`, (err, files) => {
    dirFiles = files
  })
  fs.readFile(`api/files/${req.params.filename}`, 'utf-8', (err, data) => {
    if (req.params.filename !== dirFiles.find((item) => item === req.params.filename)) {
      res.status(400).json({
        message: `No file with  ${req.params.filename} found`,
      })
    } else {
        if (+req.params.password === filesArr.find((item) => item.filename === req.params.filename)?.password) {
          fs.stat(`api/files/${req.params.filename}`, (err, stats) => {
            birthtime = stats.birthtime
            if (err) {
              res.status(500).json({
                message: err.message,
              })
            } else {
              res.status(200).json({
                message: 'Success',
                filename: req.params.filename,
                content: data,
                extension: req.params.filename.split('.').pop(),
                uploadedDate: birthtime,
              })
            }
          })
        } else {
          res.status(400).json({
            message: `Wrong password`,
          })
        }
    }
  })
})

router.get('/:filename', (req, res) => {
  let birthtime
  fs.readdir(`api/files`, (err, files) => {
    dirFiles = files
  })
  fs.readFile(`api/files/${req.params.filename}`, 'utf-8', (err, data) => {
    if (req.params.filename !== dirFiles.find((item) => item === req.params.filename)) {
      res.status(400).json({
        message: `No file with  ${req.params.filename} found`,
      })
    } else if (filesArr.find((item) => item.filename === req.params.filename)?.password) {
        res.status(400).json({
          message: `Please enter a password for ${req.params.filename} file`,
        })
    } else {
      fs.stat(`api/files/${req.params.filename}`, (err, stats) => {
        birthtime = stats.birthtime
        if (err) {
          res.status(500).json({
            message: err.message,
          })
        } else {
          res.status(200).json({
            message: 'Success',
            filename: req.params.filename,
            content: data,
            extension: req.params.filename.split('.').pop(),
            uploadedDate: birthtime,
          })
        }
      })
    }
  })
})

router.get('/', (req, res) => {
  fs.readdir(`api/files`, (err, files) => {
    if (err) {
      res.status(500).json({
        message: err.message,
      })
    } else {
      res.json({
        message: 'Success',
        files: files,
      })
    }
  })
})

router.post('/', (req, res) => {
  const fileName = req.body.filename
  if (fileName === undefined) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    })
  } else if (req.body.content === undefined) {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    })
  } else if (
    fileName.split('.').pop() === 'txt' ||
    fileName.split('.').pop() === 'log' ||
    fileName.split('.').pop() === 'json' ||
    fileName.split('.').pop() === 'yaml' ||
    fileName.split('.').pop() === 'xml' ||
    fileName.split('.').pop() === 'js'
  ) {
    filesArr.push({ filename: fileName, password: req.body.password })
    fs.writeFile(`api/files/${fileName}`, req.body.content, 'utf-8', (err) => {
      if (err) {
        res.status(400).json({
          message: err.message,
        })
      } else if (req.body.content === '') {
        res.status(400).json({
          message: "Please specify 'content' parameter",
        })
      } else {
        res.status(200).json({
          message: 'File created successfully',
        })
      }
    })
  } else {
    res.status(400).json({
      message: 'Wrong file extensions',
    })
  }
})

router.put('/:filename', (req, res) => {
  fs.readdir(`api/files`, (err, files) => {
    dirFiles = files
  })
  fs.readFile(`api/files/${req.params.filename}`, 'utf-8', (err, data) => {
    if (
      req.params.filename !==
      dirFiles.find((item) => item === req.params.filename)
    ) {
      res.status(400).json({
        message: `No file with  ${req.params.filename} found`,
      })
    } else {
        fs.writeFile(
          `api/files/${req.params.filename}`,
          req.body.content,
          'utf-8',
          (err) => {
            if (err) {
              res.status(500).json({
                message: err.message,
              })
            } else if (req.body.content === '') {
              res.status(400).json({
                message: "Please specify 'content' parameter",
              })
            } else {
              res.status(200).json({
                message: 'File updated successfully',
              })
            }
          }
        )
    }
  })
})

router.delete('/:filename', (req, res) => {
  fs.readdir(`api/files`, (err, files) => {
    dirFiles = files
  })
  fs.readFile(`api/files/${req.params.filename}`, 'utf-8', (err, data) => {
    if (
      req.params.filename !==
      dirFiles.find((item) => item === req.params.filename)
    ) {
      res.status(400).json({
        message: `No file with  ${req.params.filename} found`,
      })
    } else {
      
        fs.unlink(`api/files/${req.params.filename}`, (err) => {
          if (err) {
            res.status(500).json({
              message: err.message,
            })
          } else {
            filesArr = filesArr.filter(
              (item) => item.filename !== req.params.filename
            )
            res.status(200).json({
              message: `${req.params.filename} file was deleted`,
            })
          }
        })
    }
  })
})

module.exports = router
