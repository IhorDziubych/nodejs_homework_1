const express = require('express')
const app = express()
const PORT = 8080
const fileRouter = require('./fileRouter')

app.use(express.json())

app.use((req, res, next) => {
  console.log(`${req.method} ${req.url}`, req.body)
  next()
});

app.use('/api/files', fileRouter)

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});